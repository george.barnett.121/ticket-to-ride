"""Links between cities"""
# std
import logging
from typing import Tuple

# packages
from dataclasses import dataclass

# internal
from tickettoride.core.colours import LinkColours

# globals
_logger = logging.getLogger(__name__)

# TODO GB: unsure how we identify players or cities up to now
CityId = str
PlayerId = str


@dataclass
class Link:
    """One Link is a single route from a city to city. There can be multiple routes per city"""

    cities: Tuple[CityId, CityId]
    length: int
    colour: LinkColours
    tunnel: bool = False
    station: PlayerId | None = None
    owner: PlayerId | None = None
