"""Main class for ticket to ride"""
# std
from collections import defaultdict
import itertools
import logging
from pathlib import Path
from typing import Dict, List, Sequence, Set

# packages
from tickettoride.core.colours import LinkColours
from tickettoride.core.exceptions import LinkNotExist, LinkCannotSupportStation

# internal
from tickettoride.core.links import Link, CityId, PlayerId

# globals
_logger = logging.getLogger(__name__)

ParrallelLinks = Dict[LinkColours, Link]  # Multiple Links can be between two cities
Network = Dict[CityId, Dict[CityId, ParrallelLinks]]
Route = List[Link]  # City->City->City through links


class Board:
    def __init__(self, cities: Set[CityId], links: Sequence[Link]) -> None:
        cities: set[CityId] = set(cities)

        link_cities = set(itertools.chain(*{link.cities for link in links}))
        if cities.difference(link_cities):
            raise ValueError(
                "Cities do match cities in links:"
                f"{cities.difference(link_cities)}. Either a city does not appear in 0"
                "the links or a link contains a city that does not appear in cities"
            )
        self.network: Network = self._build_network(links)

    def _build_network(self, links: Sequence[Link]) -> Network:
        """Build up a lookup table of the links

        This should be mirrored, e.g. London -> paris should be the same as
        Paris -> Lonon. Each city to city can also have multiple connections, hence why each Route can
        have multiple links (and so contains an array of links)
        """
        cities: Network = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        for link in links:
            c1, c2 = link.cities
            # Create mirrored connections
            cities[c1][c2][link.colour] = link
            cities[c2][c1][link.colour] = link
        return cities

    def place_link(
        self, player: PlayerId, city1: CityId, city2: CityId, colour: LinkColours
    ):
        """Place a link on a route between two cities"""
        _logger.debug(
            f"Player {player} placing a {colour} link between {city1} and {city2}."
        )
        # Get the target link by looking up the two cities and matching by link colour
        try:
            target_link = self.network[city1][city2][colour]
        except KeyError as e:
            raise LinkNotExist() from e
        target_link.owner = player
        _logger.info(f"{player} placeda station between {city1} and {city2}")

    def place_station(
        self, player: PlayerId, city1: CityId, city2: CityId, colour: LinkColours
    ) -> None:
        """Place a station on an owned route between two cities"""
        _logger.debug(
            f"Player {player} placing a station between {city1} and {city2} on {colour}."
        )
        # Get the target link by looking up the two cities and matching by link colour
        try:
            target_link = self.network[city1][city2][colour]
        except KeyError as e:
            raise LinkNotExist() from e
        if not target_link.owner:
            raise LinkCannotSupportStation()
        target_link.station = player
        _logger.info(f"{player} placed a station between {city1} and {city2}")

    def owned_connection(
        self,
        player: PlayerId,
        city1: CityId,
        city2: CityId,
        include_stations: bool = True,
    ) -> Route:
        """Check if the player can create a connection between two cities

        Does not guarantee the shortest route.
        TODO: Implement Djikstra, and caching/hashing of the results

        player: Player to search connection
        city1: start city
        city2: end city
        include_stations: whether to include stations in the links
        """
        return []

    def available_connection(
        self,
        city1: CityId,
        city2: CityId,
        include_player: PlayerId | None = None,
    ) -> Route:
        """Get a unowned connection between two cities

        Does not guarantee the shortest route.
        TODO: Implement Djikstra, caching/hashing

        city1: start city
        city2: end city
        include_player: whether to include a specific player in the links
        """
        return []

    @classmethod
    def from_board(cls, board: "Board") -> "Board":
        _logger.info("Loading from board")
        return board

    @classmethod
    def from_json(cls, file: Path) -> "Board":
        _logger.info(f"Loading from json, {file}")
        return Board()

    @classmethod
    def from_default(cls) -> "Board":
        """Load standard game

        Lookup the standard game json and load this as the board
        """
        _logger.info("Loading default game")
        return Board()
