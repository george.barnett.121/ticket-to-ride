class BaseTicketToRideException(Exception):
    """Base class for exceptions"""


class AlreadyOwned(BaseTicketToRideException):
    """Link or station is already owned"""

    pass


class LinkNotExist(BaseTicketToRideException):
    """Link specified does not exist"""

    pass


class LinkCannotSupportStation(BaseTicketToRideException):
    """No link built to place a station on"""

    pass
