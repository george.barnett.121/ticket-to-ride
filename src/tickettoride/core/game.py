"""Main class for ticket to ride"""
# std
from collections import defaultdict
from email.policy import default
import itertools
import logging
from pathlib import Path
from typing import Dict, Sequence, Set, Tuple

# packages
from dataclasses import dataclass

# internal
from tickettoride.core.links import Link, CityId


# globals
_logger = logging.getLogger(__name__)

Route = Sequence[Link]
Network = Dict[CityId, Dict[CityId, Route]]


class Engine:
    def __init__(self, board: Board | None) -> None:
        ...

    def apply_action():
        pass


class State:
    def __init__() -> None:
        self.board: Board = Board()
