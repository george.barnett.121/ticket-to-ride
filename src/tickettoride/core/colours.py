"""Links between cities"""
# std
from enum import Enum, auto
import logging

# packages
# n/a

# internal
# n/a

# globals
_logger = logging.getLogger(__name__)

# TODO GB: Figure how to subclass these and make grey work for anything


class CardColours(Enum):
    RED = auto()
    GREEN = auto()
    BLUE = auto()
    LOCOMOTIVE = auto()


class LinkColours(Enum):
    RED = auto()
    GREEN = auto()
    BLUE = auto()
    GREY = auto()
