"""Functions for graph theory"""


from math import inf
from typing import Any, Callable, Dict, Hashable, List, NamedTuple, Tuple

Node = Hashable
Edge = Any
MultiEdge = Dict[Any, Edge]
Graph = Dict[Node, Dict[Node, MultiEdge]]
Path = List[Edge]


class Path(NamedTuple):
    length: int | float
    path: Path


def dijkstra(
    graph: Graph,
    start_node: Node,
    edge_cost: Callable[[Edge], int | float] = None,
) -> Dict[Node, Path]:
    """Implement dijkstra but return shortest path to all nodes.

    vertox_cost: Function that calculate cost of the vertex
    """

    if edge_cost is None:
        # Default is to treat all routes the same cost
        edge_cost = lambda link: 1
    nodes_left = set(graph.keys())

    nodes_visited: set[Node] = set()
    # Start all nodes at infinite distance, except starting city, which starts at 0
    # distances can be int or float to support infinty
    distances: Dict[Node, Path] = {node: Path(inf, []) for node in nodes_left}
    distances[start_node] = Path(0, [])

    while nodes_left:
        distances_left = {c: distances[c][0] for c in nodes_left}
        closest_node: Node = min(distances_left, key=distances_left.get)
        nodes_left.remove(closest_node)

        for neighbour in graph[closest_node]:
            nodes_visited.add(neighbour)
            if not graph[closest_node][neighbour]:
                continue
            # Don't assume that all colours are the same length
            neighbour_to_closet: Edge = min(
                graph[closest_node][neighbour].values(), key=edge_cost
            )
            possible_shorter_distance: int | float = distances[closest_node][
                0
            ] + edge_cost(neighbour_to_closet)
            possible_shorter_route = distances[closest_node][1] + [neighbour_to_closet]

            if possible_shorter_distance < distances[neighbour][0]:
                distances[neighbour] = Path(
                    possible_shorter_distance,
                    possible_shorter_route,
                )
    assert set(nodes_visited) == set(graph.keys())

    return distances
