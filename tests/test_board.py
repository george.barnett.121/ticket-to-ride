import pytest


from tickettoride.core.colours import LinkColours
from tickettoride.core.board import Board
from tickettoride.core.links import Link


def test_yes():
    assert True


def test_board():
    city1, city2, city3 = "London", "Paris", "Amsterdam"
    cities = [city1, city2, city3]
    links = [
        Link((city1, city2), length=3, colour=LinkColours.RED),
        Link((city3, city2), length=1, colour=LinkColours.BLUE),
        Link((city3, city1), length=2, colour=LinkColours.RED),
        Link((city3, city1), length=2, colour=LinkColours.BLUE),
    ]

    board = Board(cities, links)
    assert board.network

    player1, player2 = "Alice", "Bob"
    board.place_link(player1, city1, city2, LinkColours.RED)
    board.place_link(player2, city1, city3, LinkColours.BLUE)
    # Check reverse
    assert board.network[city1][city2][LinkColours.RED].owner == player1
    assert board.network[city2][city1][LinkColours.RED].owner == player1

    # Check none station
    assert board.network[city1][city3][LinkColours.BLUE].owner == player2
    assert board.network[city1][city3][LinkColours.BLUE].station is None

    # Check station
    board.place_station(player2, city1, city2, LinkColours.RED)
    assert board.network[city1][city2][LinkColours.RED].station == player2
