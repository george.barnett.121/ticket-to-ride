import pytest

from tickettoride.core.graphs import dijkstra
from tickettoride.core.links import LinkColours, Link
from tickettoride.core.board import Board


@pytest.fixture
def basic_cities():
    yield ["Amsterdam", "Berlin", "Cairo", "Denver"]

@pytest.fixture
def complex_cities():
    yield ["Amsterdam", "Berlin", "Cairo", "Denver","Edinburgh","Fes"]

@pytest.fixture
def basic_board(basic_cities):
    """Basic Diamond pattern
    
    A -3- B -2- D
     \-5- C -1-/
    """
    cityA, cityB, cityC, cityD = basic_cities
    links = [
        Link((cityA, cityB), length=3, colour=LinkColours.RED),
        Link((cityA, cityC), length=5, colour=LinkColours.RED),
        Link((cityB, cityD), length=2, colour=LinkColours.RED),
        Link((cityC, cityD), length=1, colour=LinkColours.RED),
    ]

    yield Board(basic_cities, links)


@pytest.fixture
def complex_board(complex_cities):
    """Basic Diamond pattern
    
    A -3- B -2- D -4-\ 
     \-5- C -1-/       F
           \-1- E 1-/
    """
    cityA, cityB, cityC, cityD,cityE, cityF  = complex_cities
    links = [
        Link((cityA, cityB), length=3, colour=LinkColours.RED),
        Link((cityA, cityC), length=5, colour=LinkColours.BLUE),
        Link((cityB, cityD), length=2, colour=LinkColours.RED),
        Link((cityC, cityD), length=1, colour=LinkColours.BLUE),
        Link((cityC, cityE), length=1, colour=LinkColours.BLUE),
        Link((cityD, cityF), length=4, colour=LinkColours.RED),
        Link((cityD, cityF), length=4, colour=LinkColours.BLUE),
        Link((cityE, cityF), length=1, colour=LinkColours.RED),
    ]

    yield Board(complex_cities, links)
