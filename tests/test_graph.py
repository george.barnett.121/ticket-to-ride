from math import inf

import pytest

from tickettoride.core.graphs import dijkstra
from tickettoride.core.links import LinkColours, Link
from tickettoride.core.board import Board


def test_dijkstra(basic_cities, basic_board):
    city1 = basic_cities[0]
    target_city = basic_cities[-1]
    network = basic_board.network

    results = dijkstra(
        network,
        city1,
    )
    assert results[target_city].length == 2
    assert len(results[target_city].path) == 2


def test_dijkstra_length_cost(basic_cities, basic_board):
    city1 = basic_cities[0]
    target_city = basic_cities[-1]
    network = basic_board.network

    results = dijkstra(network, city1, edge_cost=lambda x: x.length)


def test_dijkstra_length_complex(complex_cities, complex_board):
    city1 = complex_cities[0]
    target_city = complex_cities[-1]
    network = complex_board.network

    results = dijkstra(network, city1, edge_cost=lambda x: x.length)
    assert results[target_city].length == 7


def test_dijkstra_red_only(complex_cities, complex_board):
    city1 = complex_cities[0]
    network = complex_board.network

    def red_only(x):
        return 1 if x.colour == LinkColours.RED else inf

    results = dijkstra(network, city1, edge_cost=red_only)
    assert results["Cairo"].length == inf
    assert results["Fes"].length == 3
